import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';


@Injectable()
export class ApiserviceProvider {

  constructor(public http: Http, public sqlite: SQLite) {
    console.log('Hello ApiserviceProvider Provider');
  }
  callServer(serviceUrl):Promise<any>
  {
      let response : Promise<any>;
      let headers = new Headers({ 'Content-Type': 'application/json'});
      let options = new RequestOptions({ headers: headers });
      response = this.http.get(serviceUrl,options).toPromise().then(responseData => responseData).catch(err => this.errorDisplay(err));
      return response;
  }
  errorDisplay(error: any): Promise<any>{
      return Promise.reject(error.message || error);
  }

  GenerateDb()
  {
    this.sqlite.create({
      name: 'MyNavigationDatabase.db',
      location: 'default'
    }).then((db: SQLiteObject)=>{
      db.executeSql('CREATE TABLE IF NOT EXISTS myfavourites(rowid INTEGER PRIMARY KEY, tagname VARCHAR(100), latlngname VARCHAR(100), navDate );', [])
      .then((Success) => {console.log('Executed SQL');})
      .catch((e) => {console.log(e);});
    }).catch((e) => {console.log(e);});
  }
}
