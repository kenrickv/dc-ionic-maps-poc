import { Injectable } from "@angular/core";
import { INearbyOptions } from "../../components/nearby-locations/nearby-location";

@Injectable()
export class AppData{

    constructor(){

    }

    get maps_key(): string{
        return 'AIzaSyC8IZv0hS7mQsGZPj3S3g4ycSeX88H_f_c'
    }

    get googlePlacesUrlPrefix(): string{
        return 'https://maps.googleapis.com/maps/api/place/nearbysearch/json'
    }

    get nearbyRadius(): number{
        return 200
    }

    get nearbyEntities(): Array<INearbyOptions>{
        return [{
            title: 'Train',
            type: 'train_station'
          },
          {
            title: 'Hotels',
            type: 'restaurant'
          },
          {
            title: 'Hospitals',
            type: 'host'
          }]
    }
}