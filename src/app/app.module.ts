import { PlannerDirectionsPage } from './../pages/planner-directions/planner-directions';
import { PlannerPage } from './../pages/planner/planner';
import { HomePage } from './../pages/home/home';
import { MyPlaces } from './../components/my-places/my-places';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Geolocation } from '@ionic-native/geolocation';
import { MyApp } from './app.component';
import { NavigationtrackerPage} from '../pages/navigationtracker/navigationtracker';
import { Network } from '@ionic-native/network';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { ApiserviceProvider } from '../providers/apiservice/apiservice';
import { HttpModule } from '@angular/http';
import { AppData } from './../providers/app-data/app-data';
import { SQLite } from '@ionic-native/sqlite';
import { ComponentsModule } from '../components/components.module';
import { Geofence } from '@ionic-native/geofence';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    NavigationtrackerPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ComponentsModule,
    IonicModule.forRoot(MyApp)
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    NavigationtrackerPage,
    MyPlaces
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    Geofence,
    Network,
    NativeGeocoder,
    ApiserviceProvider,
    AppData,
    SQLite,
    {provide: ErrorHandler, useClass: IonicErrorHandler}    
  ]
})
export class AppModule {}
