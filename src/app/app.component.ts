import { PlannerPage } from './../pages/planner/planner';
import { HomePage } from './../pages/home/home';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ApiserviceProvider } from '../providers/apiservice/apiservice';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Plugins } from '@capacitor/core';

const { Storage } = Plugins
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(platform: Platform, public sqlite: SQLite, statusBar: StatusBar, splashScreen: SplashScreen, public apiservice:ApiserviceProvider) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.storeDefaultTags()

  }


  storeDefaultTags(){
    let defaultTags: Array<any> = [{
        tag: 'Home',
        lat: 0.0,
        lng: 0.0,
        address: ''
      },{
        tag: 'Work',
        lat: 0.0,
        lng: 0.0,
        address: ''
      },
      {
        tag: 'Others',
        lat: 0.0,
        lng: 0.0,
        address: ''
      }
    ]

    Storage.set({key: "tags", value: JSON.stringify(defaultTags)}).then((value)=>{
      console.log('Default tags stored successfully')
    }).catch((error)=>{
      console.log('Error in saving default tags')
    })
    
  }
  
}

