import { PlannerDirectionsPage } from './../planner-directions/planner-directions';
import { Plugins } from '@capacitor/core';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

const { Storage } = Plugins

interface PlanerStructure {
  lat,
  lng,
  address
}
/**
 * Generated class for the PlannerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-planner',
  templateUrl: 'planner.html',
})
export class PlannerPage {

  source: string = ''
  destination: string = ''
  sourceResult
  destinationResult
  previousRoutes = []

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlannerPage');
    this.populateRoutes()

  }

  populateRoutes(){
    this.loadHistoricRoutes().then((savedRoutes)=>{
      console.log(savedRoutes)
      this.previousRoutes = savedRoutes!=null ? savedRoutes : []
    }).catch(error=>{
      this.previousRoutes = []
    })
  }

  handleSource(result){
    if(result){
      this.source = result.formatted_address
      this.sourceResult = result
    }
  }

  handleDestination(result){
    if(result){
      this.destination = result.formatted_address
      this.destinationResult = result
    } 
  }

  toggleSources(){
    let temp = this.source
    this.source = this.destination
    this.destination = temp

    let tempResult = this.sourceResult
    this.sourceResult = this.destinationResult
    this.destinationResult = tempResult
  }

  /**
   * Method to load saved routes from Storage with key 'routes'
   */
  loadHistoricRoutes(): Promise<any>{
    return new Promise<any>((resolve, reject)=>{
      Storage.get({ key : 'planner'}).then(data=>{
        let storedRoutes = JSON.parse(data.value)
        resolve(storedRoutes)
      }).catch(error=>{
        reject(error)
      })
    })
  }

  savePlannedRoute(){
    let sourceStruct: PlanerStructure = {
      lat: this.sourceResult.geometry.location.lat(),
      lng: this.sourceResult.geometry.location.lng(),
      address: this.sourceResult.formatted_address
    }

    let destStruct: PlanerStructure = {
      lat: this.destinationResult.geometry.location.lat(),
      lng: this.destinationResult.geometry.location.lng(),
      address: this.destinationResult.formatted_address
    }
    this.previousRoutes.push({
      source: sourceStruct,
      destination: destStruct
    })
    Storage.set( { key: 'planner',
                  value:  JSON.stringify(this.previousRoutes)})
  }

  navigateToDirections(event, route){
    event.stopPropagation()
    
    this.navCtrl.push('PlannerDirectionsPage', {
      route: route
    })
  }

  saveTag(event, route){
    event.stopPropagation()
    
  }
}
