import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlannerPage } from './planner';

@NgModule({
  declarations: [
    PlannerPage,
  ],
  imports: [
    IonicPageModule.forChild(PlannerPage),
    ComponentsModule
  ],
})
export class PlannerPageModule {}
