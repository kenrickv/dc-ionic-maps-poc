import { PlannerPage } from './../planner/planner';
import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, PopoverController } from 'ionic-angular';
import { Plugins } from '@capacitor/core';
import { MyPlaces } from '../../components/my-places/my-places';

const { Geolocation, BackgroundTask, LocalNotifications } = Plugins;

declare var google:any;
var map:any;
var infowindow: any
var geocoder: any

enum GeoCodeType {
  ADDRESS = 'address',
  PLACE = 'placeId',
  LATLNG = 'latLng'
}

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  @ViewChild('map') mapElement: ElementRef;
  googleAutoComplete
  autoComplete
  autoCompleteItems
  currentLattitude
  currentLongitude
  markers:any[] = [];
  searchText: string = 'Search'
  isSaveTagOpen: boolean = false
  
  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public ngZone:NgZone, 
              private menuCtrl: MenuController, 
              private popoverCtrl: PopoverController) {
  }

  ionViewDidLoad() {
    this.loadMap();
  }

  //Default map loading with Draggable Marker Options.
  loadMap(){

    infowindow = new google.maps.InfoWindow();
    geocoder = new google.maps.Geocoder();
    this.googleAutoComplete = new google.maps.places.AutocompleteService()
    
    //Closing menu if opened
    this.closeMenu()
    
    this.clearAutoCompleteFields()


    let mapOptions = {
      center: {lat: 19.20, lng: 19.20},
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      zoom: 12,
      mapTypeControl: false,
      streetViewControl: false,
      scaleControl: false,
      disableDefaultUI: true //Disabling default UI (Zoom controls, etc.)
    }
    map = new google.maps.Map(this.mapElement.nativeElement, mapOptions)

    this.getCurrentPosition().then((coordinates)=>{
      
      this.currentLattitude = coordinates.coords.latitude
      this.currentLongitude = coordinates.coords.longitude
      this.geoCodeAddress(GeoCodeType.LATLNG, {lat: coordinates.coords.latitude, lng: coordinates.coords.longitude}).then((result)=>{
        
        this.populateMarker(coordinates.coords.latitude, coordinates.coords.longitude, result.formatted_address, '', true, true)
        
        //Moving map to current location
        map.panTo({lat: coordinates.coords.latitude, lng: coordinates.coords.longitude})
      }).catch(error=>{
        alert('There was a problem in Geocoding your location.')
        console.log('Error in Geocoding in location.ts')
        console.log(error)
      })
    }).catch(error=>{
      alert('Not able to get your location. Please check if your location service is enabled from device settings')
      console.log('Error in Geolocation in location.ts')
      console.log(error)
    })
    
  }

  /**
   * Capacitor method to get current position
   */
  async getCurrentPosition(){
    return await Geolocation.getCurrentPosition();    
  }

  /**
   * Method returning Geocoding of address
   * @param key 
   * @param query 
   */
  geoCodeAddress(key: string, query): Promise<any>{
    return new Promise((resolve, reject)=>{
      geocoder.geocode(this.getGeocodeByType(key, query), (results, status)=>{
        if(status === 'OK' && results[0]){
          
          resolve(results[0])
        }
        else{
          reject(new Error('No location found in geocoding'))
        }
      })
    })
  }

  /**
   * Method to create and populate marker with infowindow based on taggable handling
   * @param lat 
   * @param lng 
   * @param name 
   * @param vicinity 
   * @param isDraggable 
   * @param isTaggable 
   */
  populateMarker(lat, lng, name, vicinity, isDraggable: boolean, isTaggable: boolean): any{

    let latlng = new google.maps.LatLng(lat, lng)
    
    let infoWindow = new google.maps.InfoWindow({
      content: isTaggable ? '<div><div class="infoWindow">' + name +'</div><button class="infoWindowButton" ion-button block full id="tagButton">Tag</button></div>' : name
    })

    let marker = new google.maps.Marker({
      map: map,
      animation: google.maps.Animation.DROP,
      position: latlng,
      draggable: isDraggable,
      infowindow: infoWindow
    })
    marker.addListener('click', ()=>{
      this.closeAllInfoWindows()
      
      if(!isTaggable){
        map.setCenter(marker.getPosition())
        this.smoothZoom(map, 20, map.getZoom())  
      }
      
      infoWindow.addListener('domready', ()=>{
        if(isTaggable){
          document.getElementById("tagButton").addEventListener('click', ()=>{
        
            if(!this.isSaveTagOpen)
              this.saveTag(marker, name)
          });
        }
      });
      infoWindow.open(map, marker)
    })

    marker.addListener('dragend', ()=> {
      this.closeAllInfoWindows()

      this.geoCodeAddress(GeoCodeType.LATLNG, marker.getPosition()).then((result)=>{
          let infoWindowContent = !isTaggable ? name : '<div><div class="infoWindow">' + result.formatted_address +'</div><button class="infoWindowButton" ion-button block full id="tagButton">Tag</button></div>'
          infowindow.setContent(infoWindowContent);
          if(isTaggable){
            infowindow.addListener('domready', ()=>{
              document.getElementById("tagButton").addEventListener('click', ()=>{
                
                if(!this.isSaveTagOpen)
                  this.saveTag(marker, result.formatted_address)
              });
            });
          }
          infowindow.open(map,marker);
      })
    

    });

    this.markers.push(marker);
    
}

/**
 * Smooth Zoom to position
 * @param map - map to zoom
 * @param max - Max Zoom level
 * @param cnt - Continued level
 */
smoothZoom(map, max, cnt){
  
  if(cnt >= max){
    return
  }
  else{
    let z = google.maps.event.addListener(map, 'zoom_changed', (event)=>{
      google.maps.event.removeListener(z)
      this.smoothZoom(map, max, cnt+1)
    })
    setTimeout(() => {
      map.setZoom(cnt)
    }, 80);
  }
}

// Sets the map on all markers in the array.
setMapOnAll(map) {
  for(let i = 0; i < this.markers.length; i++) {
    this.markers[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
clearMarkers() {
  this.setMapOnAll(null);
}

// Deletes all markers in the array by removing references to them.
deleteMarkers() {

  //Need to delete previous marker set, Not the solution but workaround
  //marker.setMap(null)

  this.clearMarkers();
  this.markers = [];
}

closeAllInfoWindows(){
  this.markers.forEach((marker)=>{
    marker.infowindow.close(map, marker)
  })
}

closeMenu(){
  this.menuCtrl.close()
}

clearAutoCompleteFields(){
  this.autoComplete = { input: ''}
  this.autoCompleteItems = []
}

getGeocodeByType(type, query){
  switch(type){
    case GeoCodeType.ADDRESS:
      return {'address' : query}

    case GeoCodeType.PLACE:
      return {'placeId' : query}

    case GeoCodeType.LATLNG:
      return {'latLng': query}
  }
}

 /**
   * Method to geocode address on item selected from GoogleAutoCompleteSearchView
   * @param item 
   */
  onSearchSelected(result){

    if(result){
      this.deleteMarkers()
      this.populateMarker(result.geometry.location.lat(), result.geometry.location.lng(), result.formatted_address, '', true, true)
      map.setCenter(result.geometry.location)
     
    }
    
  }

  /**
   * Method to open 'MyPlaces' component as pop-over to save tag
   * @param marker 
   * @param address 
   */
  saveTag(marker, address: string){

    this.isSaveTagOpen = true
    let popoverOptions = {
      showBackdrop: true,
      enableBackdropDismiss: true
    }
    let popover = this.popoverCtrl.create(MyPlaces, {
      marker: marker,
      address: address
    }, popoverOptions)
    popover.present()
    popover.onDidDismiss((data)=>{
      
      this.isSaveTagOpen = false
      if(data!=undefined){
        //this.geoFenceTaggedLocation(data.tag, marker, 100, 3, address)
        //this.geoFenceTaggedLocation1(marker)
      }  
    })
  }
  
  navigateToPage(page: string, addToBackStack){
    this.navCtrl.push(page)
  }
}
