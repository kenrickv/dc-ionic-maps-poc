import { Plugins } from '@capacitor/core';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { INearbyEntitiesOptions } from '../../components/nearby-locations/nearby-location';

/**
 * Generated class for the PlannerDirectionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google:any;
var map:any;
var infowindow: any
const { Geolocation } = Plugins

@IonicPage()
@Component({
  selector: 'page-planner-directions',
  templateUrl: 'planner-directions.html',
})
export class PlannerDirectionsPage {

  @ViewChild('map') mapElement: ElementRef;
  navigationRoute
  directionsService
  directionsDisplay
  currentLattitude
  currentLongitude
  nearbyEntities: INearbyEntitiesOptions = {
    title: 'Nearby Places',
    message: 'Please select an option',
    options: [{
      title: 'Railways',
      type: 'train_station'
    },
    {
      title: 'Shoppings',
      type: 'supermarket'
    },
    {
      title: 'Restaurants',
      type: 'restaurant'
    },
    {
      title: 'Hospitals',
      type: 'hospital'
    },
    {
      title: 'Schools',
      type: 'school'
    },
    {
      title: 'ATM',
      type: 'atm'
    }]
  }
  nearbyEntitySelected: string = ''
  markers:any[] = [];
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.navigationRoute = this.navParams.get('route')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlannerDirectionsPage');
    this.initializeDirections()
    this.navigateDirection()
  }

  initializeDirections(){
    infowindow = new google.maps.InfoWindow();
    this.directionsService = new google.maps.DirectionsService();
    this.directionsDisplay = new google.maps.DirectionsRenderer();

    let mapOptions = {
      center: {lat: 19.20, lng: 19.20},
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      zoom: 12,
      mapTypeControl: false,
      streetViewControl: false,
      scaleControl: false,
      disableDefaultUI: true //Disabling default UI (Zoom controls, etc.)
    }
    map = new google.maps.Map(this.mapElement.nativeElement, mapOptions)

    Geolocation.getCurrentPosition().then((coordinates)=>{
      this.currentLattitude = coordinates.coords.latitude
      this.currentLongitude = coordinates.coords.longitude
    }).catch(error=>{
      alert('Not able to get your location. Please check if your location service is enabled from device settings')
    })
  }

  navigateDirection(){

    
    this.directionsDisplay.setMap(map);
    var start = this.navigationRoute.source.address;
    var end = this.navigationRoute.destination.address;
    var request = {
      origin: start,
      destination: end,
      travelMode: 'WALKING'
    };
    
    this.directionsService.route(request, (result, status)=> {
      if (status === 'OK') {
        this.directionsDisplay.setDirections(result);
      }
    });
    
  }

  /**
   * Callback method on getting nearby places
   * @param nearbyPlaces 
   */
  onGettingNearbyPlaces(nearbyPlaces){
   
    this.removeDirections()
    this.deleteMarkers()

    if(nearbyPlaces instanceof Error != true){

        this.nearbyEntitySelected = 'Showing Nearby ' + nearbyPlaces.entity.title

        for(let i=0; i<nearbyPlaces.data.length;i++){
          let nearbyEntry = nearbyPlaces.data[i]
          this.populateMarker(nearbyEntry.geometry.location.lat, nearbyEntry.geometry.location.lng,
                            nearbyEntry.name, nearbyEntry.vicinity, false, false)
        }

        if(nearbyPlaces.data!=undefined){
          if(this.markers[0]){
            map.setCenter(this.markers[0].getPosition())
            map.setZoom(12)
          }      
        }
    }
    else{
      console.log(nearbyPlaces)
      this.nearbyEntitySelected = ''
      
      //Show error message
      alert('No nearby searched places found based on your current location')
      //alert(nearbyPlaces.message)
    }
  }

   /**
   * Method to create and populate marker with infowindow based on taggable handling
   * @param lat 
   * @param lng 
   * @param name 
   * @param vicinity 
   * @param isDraggable 
   * @param isTaggable 
   */
  populateMarker(lat, lng, name, vicinity, isDraggable: boolean, isTaggable: boolean): any{

    let latlng = new google.maps.LatLng(lat, lng)
    
    let infoWindow = new google.maps.InfoWindow({
      content: name
    })

    let marker = new google.maps.Marker({
      map: map,
      animation: google.maps.Animation.DROP,
      position: latlng,
      draggable: isDraggable,
      infowindow: infoWindow
    })
    marker.addListener('click', ()=>{
      this.closeAllInfoWindows()
      
      if(!isTaggable){
        map.setCenter(marker.getPosition())
        console.log('Zoomable')
        this.smoothZoom(map, 20, map.getZoom())  
      }
      
      /*infoWindow.addListener('domready', ()=>{
        if(isTaggable){
          document.getElementById("tagButton").addEventListener('click', ()=>{
            console.log('Is save tag opened: ' + this.isSaveTagOpen)
            if(!this.isSaveTagOpen)
              this.saveTag(marker, name)
          });
        }
      });
      infoWindow.open(map, marker)
    })

    marker.addListener('dragend', ()=> {
      this.closeAllInfoWindows()

      this.geoCodeAddress(GeoCodeType.LATLNG, marker.getPosition()).then((result)=>{
          let infoWindowContent = !isTaggable ? name : '<div><div class="infoWindow">' + result.formatted_address +'</div><button class="infoWindowButton" ion-button block full id="tagButton">Tag</button></div>'
          infowindow.setContent(infoWindowContent);
          if(isTaggable){
            infowindow.addListener('domready', ()=>{
              document.getElementById("tagButton").addEventListener('click', ()=>{
                console.log('Is save tag opened: ' + this.isSaveTagOpen)
                if(!this.isSaveTagOpen)
                  this.saveTag(marker, result.formatted_address)
              });
            });
          }
          infowindow.open(map,marker);
      })*/
      infoWindow.open(map, marker)

    });

    this.markers.push(marker);
    
}

/**
 * Smooth Zoom to position
 * @param map - map to zoom
 * @param max - Max Zoom level
 * @param cnt - Continued level
 */
smoothZoom(map, max, cnt){
  
  if(cnt >= max){
    return
  }
  else{
    let z = google.maps.event.addListener(map, 'zoom_changed', (event)=>{
      google.maps.event.removeListener(z)
      this.smoothZoom(map, max, cnt+1)
    })
    setTimeout(() => {
      map.setZoom(cnt)
    }, 80);
  }
}

// Sets the map on all markers in the array.
setMapOnAll(map) {
  for(let i = 0; i < this.markers.length; i++) {
    this.markers[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
clearMarkers() {
  this.setMapOnAll(null);
}

// Deletes all markers in the array by removing references to them.
deleteMarkers() {

  //Need to delete previous marker set, Not the solution but workaround
  //marker.setMap(null)

  this.clearMarkers();
  this.markers = [];
}

closeAllInfoWindows(){
  this.markers.forEach((marker)=>{
    marker.infowindow.close(map, marker)
  })
}

removeDirections(){
  this.directionsDisplay.setMap(null)
}
}
