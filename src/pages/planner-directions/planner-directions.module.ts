import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlannerDirectionsPage } from './planner-directions';

@NgModule({
  declarations: [
    PlannerDirectionsPage,
  ],
  imports: [
    IonicPageModule.forChild(PlannerDirectionsPage),
    ComponentsModule
  ],
  exports:[
    PlannerDirectionsPage
  ]
})
export class PlannerDirectionsPageModule {}
