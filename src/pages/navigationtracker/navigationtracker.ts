import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController,ModalController } from 'ionic-angular';

/**
 * Generated class for the NavigationtrackerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-navigationtracker',
  templateUrl: 'navigationtracker.html',
})
export class NavigationtrackerPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl:ViewController) {
  }
  serviceType:any;

  ionViewDidLoad() {
    console.log('ionViewDidLoad NavigationtrackerPage');
  }

  closeExplore(){
    this.viewCtrl.dismiss(this.serviceType);
    console.log("Modal closed");
  }

}
