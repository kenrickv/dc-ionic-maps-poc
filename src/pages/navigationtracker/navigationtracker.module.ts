import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NavigationtrackerPage } from './navigationtracker';

@NgModule({
  declarations: [
    NavigationtrackerPage,
  ],
  imports: [
    IonicPageModule.forChild(NavigationtrackerPage),
  ],
})
export class NavigationtrackerPageModule {}
