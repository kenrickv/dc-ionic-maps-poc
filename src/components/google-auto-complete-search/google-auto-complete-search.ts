import { Component, NgZone, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';

var geocoder: any
declare var google:any;
/**
 * Generated class for the GoogleAutoCompleteSearchComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'google-auto-complete-search',
  templateUrl: 'google-auto-complete-search.html'
})
export class GoogleAutoCompleteSearchComponent {

  googleAutoComplete
  @Input('searchPlaceholder') searchPlaceholder: string = 'Search....'
  @Input('source') source: string = ''
  autoCompleteItems
  autoCompleteData
  @Output('onSearchSelected') handleSearch = new EventEmitter()

  constructor(private ngZone: NgZone) {
    
    this.autoCompleteData = ''
    this.autoCompleteItems = []
    geocoder = new google.maps.Geocoder()
    this.googleAutoComplete = new google.maps.places.AutocompleteService()
    let element = document.getElementsByClassName('searchbar-search-icon')[0]
    
  }

  /**
   * Method called on search input change.
   * This method gets places predictions using GoogleAutoComplete API
   */
  searchPlace(){

    if(this.autoCompleteData == ''){
      this.autoCompleteItems = []
      return
    }

    this.googleAutoComplete.getPlacePredictions({input: this.autoCompleteData},
      (predictions, status)=>{
        this.autoCompleteItems = []
        this.ngZone.run(()=>{
          if(predictions!=null){
            predictions.forEach((prediction) => {
              this.autoCompleteItems.push(prediction)
            });
          }
        })
      })
  }

  /**
   * Method to geocode address on item selected from GoogleAutoCompleteSearchView
   * @param item 
   */
  onSearchSelected(item){

    this.autoCompleteData = item.description
    this.autoCompleteItems = []

    this.geoCodeAddress('placeId', item.place_id).then((result)=>{
      this.handleSearch.emit(result)
      //this.populateMarker(result.geometry.location.lat(), result.geometry.location.lng(), result.formatted_address, '', true, true)
      //map.setCenter(result.geometry.location)
    }).catch((error)=>{
      alert(error.message)
    })
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes)
    for(let propName in changes){
      let change = changes[propName]
      if(propName === 'source'){
        this.autoCompleteData = change.currentValue
      }
    }
   
  }
  /**
   * Method returning Geocoding of address
   * @param key 
   * @param query 
   */
  geoCodeAddress(key: string, query): Promise<any>{
    return new Promise((resolve, reject)=>{
      geocoder.geocode({'placeId' : query}, (results, status)=>{
        if(status === 'OK' && results[0]){
          
          resolve(results[0])
        }
        else{
          reject(new Error('No location found in geocoding'))
        }
      })
    })
  }

}
