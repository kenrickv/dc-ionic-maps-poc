import { ViewController, NavParams } from 'ionic-angular';
import { Component, ViewChild, ElementRef } from "@angular/core";
import { Plugins } from '@capacitor/core';
import { ThrowStmt } from '@angular/compiler';

const { Storage } = Plugins

@Component({
    templateUrl: 'my-places.html'
})

export class MyPlaces{

    tagOptions: Array<any> = []
    address: string = ''
    selectedTagOption: string = ''
    saveDisabled: boolean = false
    customTagName: string = ''
    
    constructor(private viewCtrl: ViewController, private navParams: NavParams){
        console.log('Passed data : ' + navParams.get('marker').getPosition())
        this.address = navParams.get('address')
        
        this.addDefaultOptions().then(defaultTagOptions=>{
            //this.tagOptions.push({title: 'Others', address: ''})
            
            if(this.tagOptions[0].address === ''){
                this.selectedTagOption = this.tagOptions[0].tag
            }
            else{
                console.log(this.tagOptions[this.tagOptions.length-1].tag)
                this.selectedTagOption = this.tagOptions[this.tagOptions.length-1].tag
            }
        })
        //this.selectedTagOption = this.tagOptions[0].title
    }

    addDefaultOptions(){
        
        return new Promise((resolve, reject)=>{

            Storage.get({key: "tags"}).then((storedkeys)=>{
                this.tagOptions = JSON.parse(storedkeys.value)
            }).catch(error=>{
                this.tagOptions = []
            })
            /*Storage.keys().then((storedKeys)=>{
                if(storedKeys.keys.length > 0){
                    let keyArray: string[] = storedKeys.keys 
                    for(let i=0; i< keyArray.length; i++){
                        Storage.get({key: keyArray[i]}).then((value)=>{
                            let parsedObj = JSON.parse(value.value)
                            this.tagOptions.push({title: parsedObj.tag, address: parsedObj.address})
                        })
                    }

                    resolve(this.tagOptions)
                }
                else{
                    reject('Please add default options in app start')
                }
            }).catch(error=>{
                reject(error)
            })*/
        })
    }

    addOtherOptions(){
        
        Storage.keys().then((storedKeys)=>{
            if(storedKeys.keys.length > 0){
                let keyArray: string[] = storedKeys.keys 
                for(let i=0; i< keyArray.length; i++){
                    Storage.get({key: keyArray[i]}).then((value)=>{
                        let parsedObj = JSON.parse(value.value)
                        this.tagOptions.push({title: parsedObj.tag, address: parsedObj.address})
                    })
                }    
            }
            this.tagOptions.push({title: 'Others', address: ''})
            
        }).catch(error=>{
            alert('Error in getting Key set')
        })

        /*if(this.tagOptions[0].address === ''){
            this.selectedTagOption = this.tagOptions[0].title
        }
        else{
            console.log(this.tagOptions[this.tagOptions.length-1].title)
            this.selectedTagOption = this.tagOptions[this.tagOptions.length-1].title
        }*/
    
    }

    ionViewDidLoad(){
        console.log('ionc view called')
    }

    onTagChanged(value){        
        this.selectedTagOption = value
        this.saveDisabled = ('others' === value.toLowerCase()) ? (this.customTagName.length > 0 ? false : true) : false
    }

    onTagEntered(event){
        this.customTagName = event.target.value
        this.saveDisabled = event.target.value.length > 0 ? false : true
    }

    onSaveClicked(){

        
        let object = {
            tag: 'others' === this.selectedTagOption.toLowerCase() ? this.customTagName : this.selectedTagOption,
            lat: this.navParams.get('marker').getPosition().lat(),
            lng: this.navParams.get('marker').getPosition().lng(),
            address: this.address
        }
        
        let item = this.tagOptions.find(v => v.tag === object.tag)
        console.log(item)
        if(item){
            let index = this.tagOptions.indexOf(item)
            this.tagOptions[index] = object
        }
        else{
            this.tagOptions.push(object)
        } 

        this.saveTag(this.tagOptions).then(()=>{
            this.viewCtrl.dismiss({
                "marker": this.navParams.get('marker')
            })
        }).catch(err=>{
            console.log(err)
        })
    }

    async saveTag(data){
        return await Storage.set({
            key: "tags",
            value: JSON.stringify(data)
        })
    }

    close(){
        this.viewCtrl.dismiss()
    }
}