import { MyPlaces } from './my-places/my-places';
import { IonicModule } from "ionic-angular/module";
import { NgModule } from '@angular/core';
import { NearbyLocationsComponent } from "./nearby-locations/nearby-location";
import { HttpModule } from '@angular/http';
import { GoogleAutoCompleteSearchComponent } from './google-auto-complete-search/google-auto-complete-search';

@NgModule({
    declarations:[
        NearbyLocationsComponent,
        MyPlaces,
        GoogleAutoCompleteSearchComponent
    ],
    imports: [
        IonicModule,
        HttpModule
    ],
    exports:[
        NearbyLocationsComponent,
        MyPlaces,
        GoogleAutoCompleteSearchComponent
    ],
    providers:[
    
    ]
})

export class ComponentsModule{

}