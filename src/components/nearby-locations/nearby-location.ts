import { AppData } from './../../providers/app-data/app-data';
import { Component, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

const { Modals } = Plugins;


export interface INearbyEntitiesOptions{
  title: string
  message?: string
  options: Array<INearbyOptions>

}

export interface INearbyOptions{
  title: string
  type: string
}

@Component({
    selector: 'nearby-locations',
    templateUrl: 'nearby-locations.html'
})


export class NearbyLocationsComponent{

    @Input('lattitude') lattitude: any = 19.0759837
    @Input('longitude') longitude: any = 72.8776559
    @Input('nearbyEntities') nearbyEntities: INearbyEntitiesOptions = {
        title: 'Nearby Locations',
        message: 'Please select nearby locations',
        options: [{
          title: 'Train',
          type: 'train_station'
        }]
    }
    @Output('nearbyPlaces') nearbyPlacesResponseEvent = new EventEmitter()

    constructor(private appData: AppData, private http: Http){
      this.nearbyEntities.options = appData.nearbyEntities
    }

    async openNearbyEntities(){
  
        Plugins.Modals.addListener('click', (event)=>{
          console.log(event)
        })
        let modal = await Modals.showActions(this.nearbyEntities)
        this.getNearbyPlaces(modal.index)
    }

    ngOnChanges(changes: SimpleChanges) {
      
      for(let propName in changes){
        let change = changes[propName]
        if(propName === 'lattitude'){
          this.lattitude = change.currentValue
        }
        if(propName === 'longitude'){
          this.longitude = change.currentValue
        }
      }
     
    }

    getNearbyPlaces(index){
      console.log(this.nearbyEntities.options[index].type)

      /*let xhr = new XMLHttpRequest()
      xhr.open('GET', "https://cors-escape.herokuapp.com/" + this.appData.googlePlacesUrlPrefix + '?'
      + 'radius=' + this.appData.nearbyRadius
      + '&key=' + this.appData.maps_key
      + '&type=' + this.nearbyEntities.options[index].type
      + '&location=' + this.lattitude+','+this.longitude, true)
      xhr.onload = ()=>{
        let text = xhr.responseText
        console.log('Text is.....')
        console.log(text)
      }
      xhr.onerror = ()=>{
        console.log('Text is Error.....') 
      }
      xhr.send()*/
      
      //Proxy by passed
      this.http.get("https://cors-escape.herokuapp.com/" + this.appData.googlePlacesUrlPrefix + '?'
                    + 'radius=' + this.appData.nearbyRadius
                    + '&key=' + this.appData.maps_key
                    + '&type=' + this.nearbyEntities.options[index].type
                    + '&location=' + this.lattitude+','+this.longitude)
                    .map((response)=>response.json())
                    .subscribe(data=>{
                      if(data.status === 'OK'){
                        this.nearbyPlacesResponseEvent.emit({
                          data: data.results,
                          entity: this.nearbyEntities.options[index]
                        })
                      }
                      else{
                        this.nearbyPlacesResponseEvent.emit(new Error(data.error_message))
                      }
                    }
                  )
    }

}