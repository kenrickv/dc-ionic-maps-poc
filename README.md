# Ionic Maps POC

# Activities:

- Install latest Ionic (Capacitor)    https://capacitor.ionicframework.com/docs/getting-started/
- Run through the Basics for Android, Web, iOS (if you have access to a MacBook)
- Run through the APIs esp the Geolocation API (https://capacitor.ionicframework.com/docs/apis/)
- PoC milestones
    - Map with a marker showing my current position
    - Map to show an address provided by the user (using a modal)
    - Display route from current location to address provided by the user 
    - Display route between two addresses provided by the user
    - Interactable markers
    - Display nearby locations
    - Geolocation tracker (ability to switch tracker on and off) to update route as user moves
    - LocalStorage to store address history (from Geolocation tracker)
    - LocalStorage to store addresses with tags (Home, Office)
    - Geofencing - local push notifications when I enter tagged addresses